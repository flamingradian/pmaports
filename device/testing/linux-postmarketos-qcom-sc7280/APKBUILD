# Maintainer: Luca Weiss <luca@lucaweiss.eu>

_flavor="postmarketos-qcom-sc7280"
pkgname=linux-$_flavor
pkgver=6.9.0
pkgrel=3
pkgdesc="Mainline Kernel fork for SC7280/SM7325/QCM6490 devices"
arch="aarch64"
_carch="arm64"
url="https://github.com/sc7280-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
	"
makedepends="bash bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_tag="v$pkgver-sc7280"

# Source
source="
	https://github.com/sc7280-mainline/$_repo/archive/refs/tags/$_tag/$_repo-$_tag.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$pkgver-sc7280"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
c0c1cddf6dc13b8b8657ebbfec822169ee877f78f5955ad9c050e4e854e18f805a18ce0740b5fdf9051541ee4136ac9fb488a0ed76d5585759c281af4d317cf5  linux-v6.9.0-sc7280.tar.gz
c3b20cdd2977af5174bc8628f08ae9423711540d92b366f0bf6d1f3cccf8d736e37ebacc63d79a6f1b8e7f2bc13cb9526613a282db349f552e3ea47c532cc48c  config-postmarketos-qcom-sc7280.aarch64
"
